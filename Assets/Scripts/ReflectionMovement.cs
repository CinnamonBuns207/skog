﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator anim;

    public float playerSpeed;
    public float runSpeed;
    private float moveLimiter = 0.75f;

    public float offsetValue;

    Vector2 movement;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = -Input.GetAxisRaw("Vertical");

        anim.SetFloat("MoveX", movement.x);
        anim.SetFloat("MoveY", movement.y);

        CameraMovement.instance.offset = new Vector3(offsetValue * movement.x, offsetValue * movement.y, -1f);

        if(movement.x == 1 || movement.x == -1 || movement.y == 1 || movement.y == -1)
        {
            anim.SetFloat("LastMoveX", movement.x);
            anim.SetFloat("LastMoveY", movement.y);
        }

        if(movement.x != 0 || movement.y != 0)
        {
            anim.SetBool("isMoving", true);
        }
        else
        {
            anim.SetBool("isMoving", false);
        }
    }

    void FixedUpdate()
    {
        if(movement.x != 0 && movement.y != 0)
        {
            movement.x *= moveLimiter;
            movement.y *= moveLimiter;
        }

        if (Input.GetKey("space"))
        {
            rb.MovePosition(rb.position + movement * runSpeed * Time.fixedDeltaTime);
        }
        else
        {
            rb.MovePosition(rb.position + movement * playerSpeed * Time.fixedDeltaTime);
        }
    }  
}
