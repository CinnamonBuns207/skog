﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    public GameObject speechBubble;
    public Animator catAnim;
    public SpriteRenderer sprite;

    bool rescuedCat = false;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }      
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Cat" && rescuedCat == false)
        {
            rescuedCat = true;
            Debug.Log("Rescued cat!");
            catAnim.SetTrigger("dogFindCat");
            speechBubble.SetActive(true);
        }

        if(other.tag == "BorderX")
        {
            CameraMovement.instance.cameraStay = false;
            CameraMovement.instance.cameraStayX = true;
        }

        if(other.tag == "BorderY")
        {
            CameraMovement.instance.cameraStay = false;
            CameraMovement.instance.cameraStayY = true;
        }

        if(other.tag == "Border")
        {
            sprite.sortingOrder = 3;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "BorderX" || other.tag == "BorderY")
        {
            CameraMovement.instance.cameraStayX = false;
            CameraMovement.instance.cameraStayY = false;
            CameraMovement.instance.cameraStay = true;
        }

        if(other.tag == "Border")
        {
            sprite.sortingOrder = 1;
        }
    }
}
